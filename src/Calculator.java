public class Calculator {


    static int addTenToNumber(int ints) {
        return ints + 10;
    }

    int add(int a, int b) {
        return a + b;
    }


    int divide(int divideWith) {
            int x = 1000 / divideWith;
            return x;
    }

    private int getArrayLength(int[] a) {
        return a.length;
    }

    public int publicGetArrayLength(int[] a) {
        int length = getArrayLength(a);
        return length;
    }

    boolean isPrime(int n) {
        if (n == 1) return false;
        else if(n == 2) return true;
        if (n%2==0) return false;
        for(int i=3;i*i<=n;i+=2) {
            if(n%i==0)
                return false;
        }
        return true;
    }
}

