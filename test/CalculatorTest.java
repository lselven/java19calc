import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CalculatorTest {

    private Calculator output;

    @BeforeEach
    void initCalculator() {
        output = new Calculator();
    }


    @Test
    @Order(1)
    @DisplayName("Testing Addition method")
    public void givenTwoNumbers_checkingIsSumEqualToBothNumbersAdded_thenTrue() {
        Assertions.assertEquals(10, output.add(5, 5));
    }

    @Test
    @Order(2)
    public void testShouldPass_IfLengthOfArrayIsTheSameAsArrayTested() {
        int[] a = new int[5];
        Assertions.assertEquals(5, output.publicGetArrayLength(a));
        Assertions.assertDoesNotThrow(()-> output.publicGetArrayLength(a));
    }

    @ParameterizedTest
    @Order(3)
    @ValueSource(ints = {1, 5, -3, 15, 1000})
    public void testsDivision(int a) {
            int result = 1000 / a;
            Assertions.assertEquals(result, output.divide(a));
    }

    @Test
    public void testsIfDivisionBy_ZeroThrowsError(){
        Assertions.assertThrows(ArithmeticException.class, () -> output.divide(0));
    }

    @ParameterizedTest
    @Order(4)
    @ValueSource(ints = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97})
    public void testShouldPass_ifNumberIsAPrime(int ints){
        Assertions.assertTrue(output.isPrime(ints));
    }

    @ParameterizedTest
    @Order(5)
    @ValueSource(ints = {1, 4, 6, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28, 30, 32, 33, 34, 35, 36})
    public void testShouldPass_ifNumberIsNotAPrime(int ints) {
        Assertions.assertFalse(output.isPrime(ints));
    }

    @ParameterizedTest
    @Order(6)
    @ValueSource(ints = {15, 6, 3})
    public void testShouldPass_ifTenIsAddedToThe_InitialValue(int ints) {
        int ten = 10;
        int sum = ten + ints;
        Assertions.assertEquals(sum, output.addTenToNumber(ints));
    }

    @AfterEach
    void tearDownCalculator() {
        output = null;
    }
}
